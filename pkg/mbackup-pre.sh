#!/bin/sh

ST_HOME=/mbackup/syncthing

exit_error() {
        echo $@
        exit 1
}

if [ ! -f $ST_HOME/config.xml ]; then
    echo Config not found, copying default config
    cp /usr/share/mbackup/syncthing/config.xml $ST_HOME/config.xml.tmp
    (
        chmod 660 $ST_HOME/config.xml.tmp
        sed -i "s/api_key_placeholder/$(uuidgen)/" $ST_HOME/config.xml.tmp
        mv $ST_HOME/config.xml.tmp $ST_HOME/config.xml
    ) || exit_error Failed to generate config
fi

if [ ! -f $ST_HOME/syncthing_pw ]; then
    echo Password not found, generating password
    uuidgen > $ST_HOME/syncthing_pw
    chmod 660 $ST_HOME/syncthing_pw
fi

