#!/bin/sh

ST_HOME=/mbackup/syncthing

echo Waiting for syncthing to start...
cnt=0
while ! nc -z localhost 8385; do
    sleep 1
    cnt=`expr $cnt + 1`
    if [ $cnt -gt 60 ]; then
        echo "Timeout waiting for syncthing to start"
        exit 1
    fi
done

st_cmd="syncthing cli --home $ST_HOME"
$st_cmd config gui password set `cat $ST_HOME/syncthing_pw` || echo WARNING: Failed to set GUI password 1>&2

