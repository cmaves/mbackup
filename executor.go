package mbackup

import (
	"context"
	"io"
	"os/exec"
	"strings"
	"syscall"
	"testing"
)

type CmdExecutor struct {
	prefix []string
}

func NewExecutor(prefix ...string) *CmdExecutor {
	if len(prefix) == 0 {
		panic("prefix must not be empty")
	}
	if len(prefix[0]) == 0 {
		panic("prefix[0] must not be empty")
	}
	return &CmdExecutor{prefix: prefix}
}

func (e *CmdExecutor) Execute(ctx context.Context, args ...string) (string, error) {
	cmdArgs := make([]string, len(e.prefix)-1+len(args))
	copy(cmdArgs, e.prefix[1:])
	copy(cmdArgs[len(e.prefix)-1:], args)
	cmd := exec.CommandContext(ctx, e.prefix[0], cmdArgs...)
	termFn := func() error {
		return cmd.Process.Signal(syscall.SIGTERM)
	}
	cmd.Cancel = termFn
	out, err := cmd.Output()
	if err != nil {
		return "", err
	}
	return string(out), nil
}

func (e *CmdExecutor) ExecuteStdoutToBuffer(ctx context.Context, writer io.Writer, args ...string) error {
	cmdArgs := make([]string, len(e.prefix)-1+len(args))
	copy(cmdArgs, e.prefix[1:])
	copy(cmdArgs[len(e.prefix)-1:], args)
	cmd := exec.CommandContext(ctx, e.prefix[0], cmdArgs...)
	cmd.Stdout = writer
	termFn := func() error {
		return cmd.Process.Signal(syscall.SIGTERM)
	}
	cmd.Cancel = termFn
	return cmd.Run()
}

type Executor interface {
	Execute(ctx context.Context, args ...string) (string, error)
	ExecuteStdoutToBuffer(ctx context.Context, writer io.Writer, args ...string) error
}

// Ensure that Executor implements Interface
var _ Executor = (*CmdExecutor)(nil)

type MockExecutorData struct {
	Stdout string
	Err    error
}

type MockExecutor struct {
	MockMap map[string]MockExecutorData
	T       *testing.T
}

func (e *MockExecutor) Execute(ctx context.Context, args ...string) (string, error) {
	s := strings.Join(args, "`")
	if v, ok := e.MockMap[s]; ok {
		return v.Stdout, v.Err
	}
	e.T.Fatalf("MockExecutor.Execute(ctx, %s): unexpected args", s)
	return "", nil // unreachable
}

func (e *MockExecutor) ExecuteStdoutToBuffer(ctx context.Context, writer io.Writer, args ...string) error {
	s := strings.Join(args, "`")
	if v, ok := e.MockMap[s]; ok {
		if _, err := io.WriteString(writer, v.Stdout); err != nil {
			e.T.Fatalf("MockExecutor.ExecuteStdoutToBuffer(ctx, f, %s) error writing: %v", s, err)
		}
		return v.Err
	}
	e.T.Fatalf("MockExecutor.ExecuteStdoutToBuffer(ctx, f, %s): unexpected args", s)
	return nil // unreachable
}

var _ Executor = (*MockExecutor)(nil)
