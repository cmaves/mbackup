package mbackup

import (
	"context"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strings"
	"time"

	"github.com/google/uuid"
)

type BtrfsProvider struct {
	executor   Executor
	backupPath string
	backupUUID uuid.UUID
}

func NewBtrfsProvider(
	ctx context.Context,
	executor Executor,
	backupPath string,
) (*BtrfsProvider, error) {
	if ctx == nil {
		ctx = context.Background()
	}

	provider := &BtrfsProvider{
		executor:   executor,
		backupPath: backupPath,
	}

	backupUUID, err := provider.GetSubvolumeUUID(ctx, backupPath, false)
	if err != nil {
		return nil, fmt.Errorf("partition path %s was not valid: %w", backupPath, err)

	}

	provider.backupUUID = backupUUID
	return provider, nil

}

var uuidRegex = regexp.MustCompile(`^\t*UUID:[ \t]*([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})$`)

func (p *BtrfsProvider) GetSubvolumeUUID(ctx context.Context, path string, pathMustMatch bool) (uuid.UUID, error) {
	args := make([]string, 0, 5)
	args = append(args, "subvolume", "show")
	if !pathMustMatch {
		out, err := p.executor.Execute(ctx, "subvolume", "get-default", path)
		if err != nil {
			return uuid.Nil, err
		}
		parts := strings.SplitN(out, " ", 3)
		idStr := parts[1]
		args = append(args, "-r", idStr)
	}
	args = append(args, path)

	out, err := p.executor.Execute(ctx, args...)
	if err != nil {
		return uuid.Nil, err
	}
	for out != "" {
		var line string
		line, out, _ = strings.Cut(out, "\n")
		match := uuidRegex.FindStringSubmatch(line)
		if match != nil {
			return uuid.MustParse(match[1]), nil
		}
	}
	return uuid.Nil, fmt.Errorf("could not find UUID in output")
}

func (p *BtrfsProvider) BackupUUID() uuid.UUID {
	return p.backupUUID
}

func getUtcTimeFromLine(line string, limit time.Time) int64 {
	parts := strings.Fields(line)

	var path string
	for i := 0; i < len(parts) && path == ""; i += 2 {
		key := parts[i]
		switch key {
		case "top", "otime":
			i++ // these keys have two splits
		case "path":
			path = parts[i+1]
		}
	}

	if len(path) < 15 {
		return -2
	}
	if len(path) > 15 {
		// check that filename is 15 characters long
		if path[len(path)-16] != '/' {
			return -2
		}
	}
	t, err := time.Parse("2006-01-02T1504", path[len(path)-15:])
	if err != nil {
		// filename is not a valid date
		return -2
	}

	if t.Before(limit) {
		return -3
	}

	return t.Unix()
}

func (p *BtrfsProvider) ListBackups(ctx context.Context, limit time.Time) ([]int64, error) {
	out, err := p.executor.Execute(ctx, "subvolume", "list", "-ro", p.backupPath)
	if err != nil {
		return nil, err
	}
	lines := strings.Split(out, "\n")
	backups := make([]int64, 0, len(lines))
	for _, line := range lines {
		t := getUtcTimeFromLine(line, limit)
		if t < 0 {
			continue
		}
		backups = append(backups, t)
	}

	sort.Sort(Int64Sorter(backups))
	return backups, nil
}

func (p *BtrfsProvider) ReceiveDiff(diffPath string, backupUtc int64) error {
	_, err := p.executor.Execute(context.Background(), "receive", "-f", diffPath, p.backupPath)
	if err != nil {
		utcStr := utcToStr(backupUtc)
		backupPath := p.backupPath + "/" + utcStr
		_, err := p.executor.Execute(context.Background(), "subvolume", "delete", backupPath)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to delete subvolume %s: %s\n", backupPath, err)
		}
	}
	return err
}

func (p *BtrfsProvider) SendDiff(parentUtc, targetUtc int64, outputPath string) error {
	args := make([]string, 0, 4)
	args = append(args, "send")
	if parentUtc >= 0 {
		parentPath := p.backupPath + "/" + utcToStr(parentUtc)
		args = append(args, "-p", parentPath)
	}
	targetPath := p.backupPath + "/" + utcToStr(targetUtc)
	args = append(args, targetPath)

	tmpFile := outputPath + ".partial"
	f, err := os.OpenFile(tmpFile, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0640)
	if err != nil {
		return err
	}
	defer f.Close()

	if err := p.executor.ExecuteStdoutToBuffer(context.Background(), f, args...); err != nil {
		if err := os.Remove(tmpFile); err != nil {
			fmt.Fprintf(os.Stderr, "Failed to delete partial diff file %s: %s\n", outputPath, err)
		}
		return err
	}
	return os.Rename(tmpFile, outputPath)
}
