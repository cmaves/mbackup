package mbackup_test

import (
	"context"
	_ "embed"
	"testing"
	"time"

	"maves.io/mbackup"

	"github.com/stretchr/testify/assert"
)

//go:embed test_data/sub_def_back.out
var subDefBackOut string

//go:embed test_data/sub_show_941_back.out
var subShow941BackOut string

//go:embed test_data/sub_list.out
var subListOut string

//go:embed test_data/sub_show.out
var subShowOut string

func TestBtrfsProvider(t *testing.T) {
	ctx := context.Background()
	mockExecutor := &mbackup.MockExecutor{
		MockMap: map[string]mbackup.MockExecutorData{
			"subvolume`get-default`/snapshots/home/cmaves": {
				Stdout: subDefBackOut,
			},
			"subvolume`show`-r`941`/snapshots/home/cmaves": {
				Stdout: subShow941BackOut,
			},
			"subvolume`show`/home/cmaves": {
				Stdout: subShowOut,
			},
			"subvolume`list`-ro`/snapshots/home/cmaves": {
				Stdout: subListOut,
			},
		},
		T: t,
	}

	provider, err := mbackup.NewBtrfsProvider(ctx, mockExecutor, "/snapshots/home/cmaves")
	assert.NoError(t, err)
	assert.Equal(t, "6a87444b-f30f-5741-8b44-eea3d1d31035", provider.BackupUUID().String())
	if t.Failed() {
		return
	}

	t.Run("TestListBackups", func(t *testing.T) {
		t2023 := time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC)
		times, err := provider.ListBackups(ctx, t2023)
		assert.NoError(t, err)
		expected := []int64{1690430580}
		assert.Equal(t, expected, times)
	})

}
