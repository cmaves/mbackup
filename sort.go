package mbackup

type Int64Sorter []int64

func (a Int64Sorter) Len() int           { return len(a) }
func (a Int64Sorter) Less(i, j int) bool { return a[i] < a[j] }
func (a Int64Sorter) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

func ListToSet[T comparable](list []T, existingSet map[T]struct{}) map[T]struct{} {
	if existingSet == nil {
		existingSet = make(map[T]struct{}, len(list))
	}
	for _, v := range list {
		existingSet[v] = struct{}{}
	}
	return existingSet
}
