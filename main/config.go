package main

import (
	"flag"
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
)

// Config is our configuration type.
type Config struct {
	SyncDir   string `yaml:"sync_dir"`
	BackupDir string `yaml:"backup_dir"`
	UseSudo   bool   `yaml:"use_sudo"`
}

var DefaultConfig = Config{
	SyncDir:   "/mbackup/syncdir",
	BackupDir: "",
	UseSudo:   true,
}

func ParseConfig(path string) *Config {
	data, err := os.ReadFile(path)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to read config file: %s\n", err)
		os.Exit(1)
	}
	config := DefaultConfig
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to parse config file: %s\n", err)
		os.Exit(1)
	}
	return &config
}

type CLIArgs struct {
	config string
}

var DefaultCLIArgs = CLIArgs{
	config: "/etc/mbackup.yaml",
}

func ParseCLIArgs() CLIArgs {
	configPath := flag.String("config", "", "Path to the configuration file")

	flag.Parse()

	args := DefaultCLIArgs

	if *configPath != "" {
		args.config = *configPath
	}

	return args
}
