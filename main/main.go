package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"maves.io/mbackup"
)

var (
	SYNC_DIR   = "/syncdir"
	BACKUP_DIR = "/snapshots/home/cmaves"
)

func main() {
	fmt.Println("Starting mbackup")

	args := ParseCLIArgs()
	config := ParseConfig(args.config)
	executorArgs := make([]string, 0, 2)
	if config.UseSudo {
		executorArgs = append(executorArgs, "/usr/bin/sudo")
	}
	executorArgs = append(executorArgs, "/usr/bin/btrfs")

	var (
		ctx, ctxCancelFn = context.WithCancel(context.Background())
		btrfsExecutor    = mbackup.NewExecutor(executorArgs...)
	)

	fmt.Println("Validating config")
	btrfsProvider, err := mbackup.NewBtrfsProvider(ctx, btrfsExecutor, config.BackupDir)
	if err != nil {
		panic(err)
	}

	go startSignalCatcher(ctx, ctxCancelFn)

	sbBackup := mbackup.NewSubvolumeBackup(config.SyncDir, btrfsProvider)
	ticker := time.NewTicker(60 * time.Second)
outer:
	for {
		fmt.Println("Starting iteration of ", config.SyncDir)
		err := sbBackup.ProcessBackups(ctx)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error processing backups: %s\n", err)
		}
		fmt.Println("Finished iteration of ", config.SyncDir)
		fmt.Println()

		select {
		case <-ticker.C:
		case <-ctx.Done():
			break outer
		}

	}

}

// / startSignalCatcher starts a signal catcher that will cancel the context when a signal is received
func startSignalCatcher(ctx context.Context, ctxCancelFn context.CancelFunc) {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	s := <-sig
	fmt.Println("Received signal", s)
	fmt.Println("Starting shutdown, Shutting down")
	ctxCancelFn()
}
