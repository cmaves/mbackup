package mbackup

import (
	"context"
	"errors"
	"os"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestFindMissingNodes(t *testing.T) {
	allNodes := []int64{1, 2, 3, 4, 5}
	testCases := map[string]struct {
		backups  []int64
		expected []int
	}{
		"AllNodesPresent": {
			backups: []int64{1, 2, 3, 4, 5},
		},
		"LastNodeMissing": {
			backups:  []int64{1, 2, 3, 4},
			expected: []int{4},
		},
		"FirstNodeMissing": {
			backups:  []int64{2, 3, 4, 5},
			expected: []int{0},
		},
		"MiddleNodeMissing": {
			backups:  []int64{1, 2, 4, 5},
			expected: []int{2},
		},
		"ConsecutiveNodesMissing": {
			backups:  []int64{1, 2, 5},
			expected: []int{2, 3},
		},
		"NonConsecutiveNodesMissing": {
			backups:  []int64{1, 3, 5},
			expected: []int{1, 3},
		},
		"BackupHasEndExtra": {
			backups: []int64{1, 2, 3, 4, 5, 6},
		},
		"BackupHasStartExtra": {
			backups: []int64{0, 1, 2, 3, 4, 5},
		},
	}
	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			missingNodes := findMissingNodes(allNodes, tc.backups)
			if len(tc.expected) == 0 {
				assert.Empty(t, missingNodes)
			} else {
				assert.Equal(t, tc.expected, missingNodes)
			}
		})
	}
}

func TestListDiffs(t *testing.T) {
	// setup
	tmpDir := t.TempDir()
	diffDir := tmpDir + "/2023-01-30T0000"
	err := os.Mkdir(diffDir, 0755)
	require.NoError(t, err)
	err = os.Mkdir(tmpDir+"/2023-02-01T0000", 0755)
	fileNames := []string{
		"bootstrap.btrfs",
		"2023-01-28T0000.btrfs",
		"2023-01-29T0000.btrfs",
		"invalid2023-01-27T0000.btrfs",
	}
	for _, filename := range fileNames {
		err := os.WriteFile(diffDir+"/"+filename, []byte{}, 0644)
		require.NoError(t, err)
	}

	t.Run("DiffsPresent", func(t *testing.T) {
		diffNames, diffsUtc, err := listDiffs(tmpDir, "2023-01-30T0000")
		assert.NoError(t, err)
		assert.Equal(t, []string{"2023-01-29T0000.btrfs", "2023-01-28T0000.btrfs", "bootstrap.btrfs"}, diffNames)
		assert.Equal(t, []int64{1674950400, 1674864000}, diffsUtc)
	})
	t.Run("NoDiffsPresent", func(t *testing.T) {
		diffNames, diffsUtc, err := listDiffs(tmpDir, "2023-02-01T0000")
		assert.NoError(t, err)
		assert.Empty(t, diffNames)
		assert.Empty(t, diffsUtc)
	})
	t.Run("DiffDirMissing", func(t *testing.T) {
		diffNames, diffsUtc, err := listDiffs(tmpDir, "2023-02-02T0000")
		assert.NoError(t, err)
		assert.Empty(t, diffNames)
		assert.Empty(t, diffsUtc)
	})

}

func TestSubvolumeBackup(t *testing.T) {
	t2023 := time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC)
	btrfsProvider := &BtrfsProvider{
		executor:   &MockExecutor{},
		backupPath: "/snapshots/home/cmaves",
		backupUUID: uuid.MustParse("6a87444b-f30f-5741-8b44-eea3d1d31035"),
	}

	t.Run("writeBackupList", func(t *testing.T) {
		tmpDir := t.TempDir()
		sb := NewSubvolumeBackup(tmpDir, btrfsProvider)
		err := sb.writeBackupList([]int64{1674864000, 1674950400})
		assert.NoError(t, err)

		data, err := os.ReadFile(tmpDir + "/6a87444b-f30f-5741-8b44-eea3d1d31035.list")
		assert.NoError(t, err)
		assert.Equal(t, "1674864000\n1674950400\n", string(data))

	})

	t.Run("findAllNodes", func(t *testing.T) {
		tmpDir := t.TempDir()
		err := os.WriteFile(tmpDir+"/6a87444b-f30f-5741-8b44-eea3d1d31035.list", []byte("1674864000\n1674950400\n"), 0644)
		require.NoError(t, err)
		err = os.WriteFile(tmpDir+"/11111111-1111-1111-1111-111111111111.list", []byte("1674864000\n1675950460\n"), 0644)
		require.NoError(t, err)
		err = os.WriteFile(tmpDir+"/22222222-2222-2222-2222-222222222222.list", []byte(""), 0644)
		require.NoError(t, err)
		err = os.WriteFile(tmpDir+"/33333333-3333-3333-3333-333333333333.list", []byte("000000000\n"), 0644)
		require.NoError(t, err)

		sb := NewSubvolumeBackup(tmpDir, btrfsProvider)
		nodes, err := sb.findAllNodes([]int64{1674950520}, t2023)
		assert.NoError(t, err)
		assert.Equal(t, []int64{1674864000, 1674950400, 1674950520, 1675950460}, nodes)
	})

	t.Run("applyOurMissing", func(t *testing.T) {
		tmpDir := t.TempDir()
		err := os.Mkdir(tmpDir+"/2023-01-28T0001", 0755)
		require.NoError(t, err)
		diffPath := tmpDir + "/2023-01-28T0001/2023-01-28T0000.btrfs"
		err = os.WriteFile(diffPath, []byte{}, 0644)
		require.NoError(t, err)

		mockMap := map[string]MockExecutorData{
			"receive`-f`" + tmpDir + "/2023-01-28T0001/2023-01-28T0000.btrfs`/snapshots/home/cmaves": {},
		}
		btrfsProvider := &BtrfsProvider{
			executor: &MockExecutor{
				MockMap: mockMap,
				T:       t,
			},
			backupPath: "/snapshots/home/cmaves",
			backupUUID: uuid.MustParse("6a87444b-f30f-5741-8b44-eea3d1d31035"),
		}

		ctx := context.Background()
		allUtcList := []int64{1674864000, 1674864060, 1674950400}
		ourBackups := []int64{1674864000, 1674950400}

		sb := NewSubvolumeBackup(tmpDir, btrfsProvider)
		ourBackups, err = sb.applyOurMissing(ctx, allUtcList, ourBackups)
		assert.NoError(t, err)
		assert.Equal(t, allUtcList, ourBackups)
	})
	t.Run("findAllMissing", func(t *testing.T) {
		tmpDir := t.TempDir()
		err := os.WriteFile(tmpDir+"/6a87444b-f30f-5741-8b44-eea3d1d31035.list", []byte("1674864000\n1674950400\n"), 0644)
		require.NoError(t, err)
		err = os.WriteFile(tmpDir+"/11111111-1111-1111-1111-111111111111.list", []byte("1674864000\n1675950460\n"), 0644)
		require.NoError(t, err)
		err = os.Mkdir(tmpDir+"/2023-07-27T0403", 0775)
		require.NoError(t, err)

		sb := NewSubvolumeBackup(tmpDir, btrfsProvider)
		allUtcList, err := sb.findAllNodes(nil, t2023)
		require.NoError(t, err)

		missingNodes, err := sb.findAllMissing(allUtcList, t2023)
		assert.NoError(t, err)
		assert.Equal(t, []int{1, 2}, missingNodes)
	})

	t.Run("writeMissingDiffs", func(t *testing.T) {
		ctx := context.Background()
		tmpDir := t.TempDir()
		allUtcList := []int64{1674864000, 1674864060, 1674950400, 1674950460, 1674950520}
		ourBackups := []int64{1674864000, 1674950400, 1674950460, 1674950520}
		missingNodes := []int{0, 2, 3, 4}
		finalErr := errors.New("unknown error occurred")
		mockMap := map[string]MockExecutorData{
			"send`/snapshots/home/cmaves/2023-01-28T0000": {
				Stdout: "bootstrap snap data\n",
			},
			"send`-p`/snapshots/home/cmaves/2023-01-29T0000`/snapshots/home/cmaves/2023-01-29T0001": {
				Stdout: "2023-01-29T0000 to 2023-01-29T0001 snap data\n",
			},
			"send`-p`/snapshots/home/cmaves/2023-01-29T0001`/snapshots/home/cmaves/2023-01-29T0002": {
				Err: finalErr,
			},
		}
		btrfsProvider := &BtrfsProvider{
			executor: &MockExecutor{
				MockMap: mockMap,
				T:       t,
			},
			backupPath: "/snapshots/home/cmaves",
			backupUUID: uuid.MustParse("6a87444b-f30f-5741-8b44-eea3d1d31035"),
		}

		sb := NewSubvolumeBackup(tmpDir, btrfsProvider)
		err := sb.writeMissingDiffs(ctx, allUtcList, ourBackups, missingNodes)
		assert.ErrorIs(t, err, finalErr)

		data, err := os.ReadFile(tmpDir + "/2023-01-28T0000/bootstrap.btrfs")
		assert.NoError(t, err)
		assert.Equal(t, "bootstrap snap data\n", string(data))
		data, err = os.ReadFile(tmpDir + "/2023-01-29T0001/2023-01-29T0000.btrfs")
		assert.NoError(t, err)
		assert.Equal(t, "2023-01-29T0000 to 2023-01-29T0001 snap data\n", string(data))
		_, err = os.Stat(tmpDir + "/2023-01-29T0002/2023-01-29T0001.btrfs")
		assert.ErrorIs(t, err, os.ErrNotExist)
		_, err = os.Stat(tmpDir + "/2023-01-29T0002/2023-01-29T0001.btrfs.partial")
		assert.ErrorIs(t, err, os.ErrNotExist)
	})
	t.Run("cleanupOldPeers", func(t *testing.T) {
		tmpDir := t.TempDir()
		err := os.WriteFile(tmpDir+"/11111111-1111-1111-1111-111111111111.list", []byte("1674864000\n1675950460\n"), 0644)
		require.NoError(t, err)
		err = os.WriteFile(tmpDir+"/22222222-2222-2222-2222-222222222222.list", []byte(""), 0644)
		require.NoError(t, err)
		err = os.Chtimes(tmpDir+"/22222222-2222-2222-2222-222222222222.list", time.Now(), time.Now().Add(-time.Hour*72))
		require.NoError(t, err)
		sb := NewSubvolumeBackup(tmpDir, btrfsProvider)

		err = sb.cleanupOldPeers()
		assert.NoError(t, err)
		_, err = os.Stat(tmpDir + "/11111111-1111-1111-1111-111111111111.list")
		assert.NoError(t, err)
		_, err = os.Stat(tmpDir + "/22222222-2222-2222-2222-222222222222.list")
		assert.ErrorIs(t, err, os.ErrNotExist)

	})
	t.Run("cleanupOldDiffs", func(t *testing.T) {
		tmpDir := t.TempDir()
		err := os.Mkdir(tmpDir+"/2023-01-28T0000", 0755)
		require.NoError(t, err)
		err = os.Mkdir(tmpDir+"/2023-01-29T0000", 0755)
		require.NoError(t, err)
		err = os.Chtimes(tmpDir+"/2023-01-28T0000", time.Now(), time.Now().Add(-time.Hour*72))
		require.NoError(t, err)
		allUtcList := []int64{1674864000, 1674864060, 1674950400, 1674950460}
		var missingNodes []int

		sb := NewSubvolumeBackup(tmpDir, btrfsProvider)
		err = sb.cleanupOldDiffs(allUtcList, missingNodes)
		assert.NoError(t, err)

		_, err = os.Stat(tmpDir + "/2023-01-28T0000")
		assert.ErrorIs(t, err, os.ErrNotExist)

	})
}

func TestUtcToString(t *testing.T) {
	utcStr := utcToStr(1674864000)
	assert.Equal(t, "2023-01-28T0000", utcStr) // make sure the TZ is correct
}

func TestDeleteWhileOpen(t *testing.T) {
	// Create a temporary directory
	tmpDir := t.TempDir()

	// Create a new file in the temporary directory
	tmpfile, err := os.Create(tmpDir + "/testfile")
	require.NoError(t, err)

	// Defer closure of file
	defer tmpfile.Close()

	// Try to delete the file while it's still open
	err = os.Remove(tmpfile.Name())
	assert.NoError(t, err)
	_, err = os.Stat(tmpfile.Name())
	assert.ErrorIs(t, err, os.ErrNotExist)
}
