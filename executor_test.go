package mbackup_test

import (
	"context"
	"os/exec"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"maves.io/mbackup"
)

func TestExecutor(t *testing.T) {
	ctx := context.Background()
	t.Run("Execute", func(t *testing.T) {
		t.Parallel()
		t.Run("SinglePrefix", func(t *testing.T) {
			t.Parallel()
			executor := mbackup.NewExecutor("echo")
			out, err := executor.Execute(ctx, "hello")
			assert.NoError(t, err)
			assert.Equal(t, "hello\n", out)
		})
		t.Run("MultiPrefix", func(t *testing.T) {
			t.Parallel()
			executor := mbackup.NewExecutor("echo", "-n")
			out, err := executor.Execute(ctx, "hello")
			assert.NoError(t, err)
			assert.Equal(t, "hello", out)
		})
		t.Run("ContextError", func(t *testing.T) {
			t.Parallel()
			ctx, cancelFn := context.WithTimeout(ctx, 200*time.Millisecond)
			defer cancelFn()
			executor := mbackup.NewExecutor("sleep", "1")
			_, err := executor.Execute(ctx)
			var exitErr *exec.ExitError
			assert.ErrorAs(t, err, &exitErr)
		})
	})

	t.Run("ExecuteStdoutToBuffer", func(t *testing.T) {
		t.Parallel()
		t.Run("SinglePrefix", func(t *testing.T) {
			t.Parallel()
			var builder strings.Builder
			executor := mbackup.NewExecutor("echo")
			err := executor.ExecuteStdoutToBuffer(ctx, &builder, "hello")
			assert.NoError(t, err)
			assert.Equal(t, "hello\n", builder.String())
		})
		t.Run("MultiPrefix", func(t *testing.T) {
			t.Parallel()
			var builder strings.Builder
			executor := mbackup.NewExecutor("echo", "-n")
			err := executor.ExecuteStdoutToBuffer(ctx, &builder, "hello")
			assert.NoError(t, err)
			assert.Equal(t, "hello", builder.String())
		})
		t.Run("ContextError", func(t *testing.T) {
			t.Parallel()
			var builder strings.Builder
			ctx, cancelFn := context.WithTimeout(ctx, 200*time.Millisecond)
			defer cancelFn()
			executor := mbackup.NewExecutor("sleep", "1")
			err := executor.ExecuteStdoutToBuffer(ctx, &builder)
			var exitErr *exec.ExitError
			assert.ErrorAs(t, err, &exitErr)
		})

	})
}
