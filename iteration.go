package mbackup

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

type SubvolumeBackup struct {
	syncDir string
	btrfs   *BtrfsProvider
}

// NewSubvolumeBackup creates a new SubvolumeBackup object.
func NewSubvolumeBackup(syncDir string, btrfs *BtrfsProvider) *SubvolumeBackup {
	return &SubvolumeBackup{
		syncDir: syncDir,
		btrfs:   btrfs,
	}
}

func (sb *SubvolumeBackup) ProcessBackups(ctx context.Context) error {
	limit := time.Now().AddDate(0, -1, 0)

	err := sb.cleanupOldPeers()
	if err != nil {
		return err
	}

	ourBackups, err := sb.btrfs.ListBackups(ctx, limit)
	if err != nil {
		return err
	}

	allUtcList, err := sb.findAllNodes(ourBackups, limit)
	if err != nil {
		return err
	}

	ourBackups, err = sb.applyOurMissing(ctx, allUtcList, ourBackups)
	if err != nil {
		return err
	}

	err = sb.writeBackupList(ourBackups)
	if err != nil {
		return err
	}

	missingNodes, err := sb.findAllMissing(allUtcList, limit)
	if err != nil {
		return err
	}

	if err = sb.writeMissingDiffs(ctx, allUtcList, ourBackups, missingNodes); err != nil {
		return err
	}

	if err = sb.cleanupOldDiffs(allUtcList, missingNodes); err != nil {
		return err
	}

	return nil
}

func (sb *SubvolumeBackup) writeBackupList(backups []int64) error {
	var dataToWrite bytes.Buffer
	dataToWrite.Grow(len(backups) * 11)
	for _, backup := range backups {
		str := strconv.FormatInt(backup, 10)
		dataToWrite.WriteString(str)
		dataToWrite.WriteByte('\n')
	}

	backupUUIDStr := sb.btrfs.BackupUUID().String()
	fileName := sb.syncDir + "/" + backupUUIDStr + ".list"
	err := os.WriteFile(fileName, dataToWrite.Bytes(), 0644)
	if err != nil {
		return err
	}
	return nil
}

var listRegex = regexp.MustCompile(`[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}.list`)

var notListFile = errors.New("File was not a list file")

func parseUtcFromFile(syncDir string, entry os.DirEntry, limit time.Time) ([]int64, error) {
	if entry.IsDir() || !listRegex.MatchString(entry.Name()) {
		return nil, notListFile
	}

	entryPath := syncDir + "/" + entry.Name()
	data, err := os.ReadFile(entryPath)
	if err != nil {
		return nil, err
	}

	limitUtc := limit.Unix()
	lines := strings.Split(string(data), "\n")
	utcList := make([]int64, 0, len(lines))
	for _, line := range lines {
		i, err := strconv.ParseInt(line, 10, 64)
		if err != nil || i < limitUtc {
			continue
		}
		utcList = append(utcList, i)
	}
	return utcList, nil
}

func (sb *SubvolumeBackup) findAllNodes(ourBackups []int64, limit time.Time) ([]int64, error) {
	entries, err := os.ReadDir(sb.syncDir)
	if err != nil {
		return nil, err
	}

	utcSet := ListToSet(ourBackups, nil)
	for _, entry := range entries {
		utcList, err := parseUtcFromFile(sb.syncDir, entry, limit)
		if err != nil && !errors.Is(err, notListFile) {
			return nil, err
		}
		for _, utc := range utcList {
			utcSet[utc] = struct{}{}
		}
	}

	utcList := make([]int64, 0, len(utcSet))
	for utc := range utcSet {
		utcList = append(utcList, utc)
	}
	sort.Sort(Int64Sorter(utcList))
	return utcList, err
}

func (sb *SubvolumeBackup) findAllMissing(allUtcList []int64, limit time.Time) ([]int, error) {
	entries, err := os.ReadDir(sb.syncDir)
	if err != nil {
		return nil, err
	}

	missingNodeSet := map[int]struct{}{}
	for _, entry := range entries {
		utcList, err := parseUtcFromFile(sb.syncDir, entry, limit)
		if errors.Is(err, notListFile) {
			continue
		}
		if err != nil {
			return nil, err
		}
		missingNodesIdxs := findMissingNodes(allUtcList, utcList)
		for _, missingNodeIdx := range missingNodesIdxs {
			missingNodeSet[missingNodeIdx] = struct{}{}
		}
	}

	missingNodeList := make([]int, 0, len(missingNodeSet))
	for missingNode := range missingNodeSet {
		missingNodeList = append(missingNodeList, missingNode)
	}
	sort.Ints(missingNodeList)
	return missingNodeList, nil

}

func (sb *SubvolumeBackup) writeMissingDiffs(ctx context.Context, allUtcList, ourBackups []int64, missingNodes []int) error {
	var (
		ourMissing    = findMissingNodes(allUtcList, ourBackups)
		ourMissingIdx int
	)

	for _, missingNode := range missingNodes {
		err := ctx.Err()
		if err != nil {
			return err
		}
		targetUtcStr := utcToStr(allUtcList[missingNode])
		fmt.Printf("A peer is missing node %s\n", targetUtcStr)
		parentNode := missingNode - 1
		for ; ourMissingIdx < len(ourMissing) && ourMissing[ourMissingIdx] < parentNode; ourMissingIdx++ {
		}
		if ourMissingIdx < len(ourMissing) && (ourMissing[ourMissingIdx] == parentNode || ourMissing[ourMissingIdx] == missingNode) {
			// we either don't have the parent or the target node
			fmt.Println("\tWe are missing the parent or target node, cannot generate diff")
			continue
		}
		var (
			diffDirPath            = sb.syncDir + "/" + targetUtcStr
			diffPath, syncTempPath string
			parentUtc              int64
		)
		if parentNode < 0 {
			parentUtc = -1
			diffPath = diffDirPath + "/bootstrap.btrfs"
			syncTempPath = diffDirPath + "/.syncthing.bootstrap.btrfs.tmp"
		} else {
			parentUtc = allUtcList[parentNode]
			parentUtcStr := utcToStr(parentUtc)
			diffPath = diffDirPath + "/" + parentUtcStr + ".btrfs"
			syncTempPath = diffDirPath + "/.syncthing." + parentUtcStr + ".btrfs.tmp"
		}
		if err := os.Mkdir(diffDirPath, 0775); err != nil && !errors.Is(err, os.ErrExist) {
			return err
		} else if err != nil {
			if exists, err := pathExists(diffPath); err != nil {
				return err
			} else if exists {
				fmt.Println("\tDiff already exist")
				continue
			}
			if exists, err := pathExists(syncTempPath); err != nil {
				return err
			} else if exists {
				fmt.Println("\tDiff is being downloaded")
				continue
			}
		}
		fmt.Println("\tGenerating diff at ", diffPath)
		if err := sb.btrfs.SendDiff(parentUtc, allUtcList[missingNode], diffPath); err != nil {
			return err
		}
	}
	return nil
}

func (sb *SubvolumeBackup) applyOurMissing(ctx context.Context, allUtcList []int64, ourBackups []int64) ([]int64, error) {
	var (
		ourBackupsNew         = ourBackups
		missingNodeIdxs []int = findMissingNodes(allUtcList, ourBackups)
		ourBackupsSet         = ListToSet(ourBackups, nil)
		err             error
	)

outer:
	for _, missingIdx := range missingNodeIdxs {
		// TODO: consider just applying every diff to see if it works
		err = ctx.Err()
		if err != nil {
			break outer
		}

		var (
			missingUtc int64 = allUtcList[missingIdx]
			utcStr           = utcToStr(missingUtc)
			diffNames  []string
			diffsUtc   []int64
		)

		fmt.Printf("Missing: %s\n", utcStr)

		diffNames, diffsUtc, err = listDiffs(sb.syncDir, utcStr)
		if err != nil {
			break outer
		}

		for i, diffName := range diffNames {
			if diffName != "bootstrap.btrfs" {
				if _, ok := ourBackupsSet[diffsUtc[i]]; !ok {
					continue
				}
			}

			diffPath := sb.syncDir + "/" + utcStr + "/" + diffName
			err = ctx.Err()
			if err != nil {
				break outer
			}
			fmt.Printf("\tApplying diff: %s\n", diffPath)
			err := sb.btrfs.ReceiveDiff(diffPath, missingUtc)
			if err == nil {
				ourBackupsSet[missingUtc] = struct{}{}
				ourBackupsNew = append(ourBackupsNew, missingUtc)
				break
			}
			fmt.Fprintf(os.Stderr, "Error applying diff %s: %s\n", diffPath, err)
		}
	}
	sort.Sort(Int64Sorter(ourBackupsNew))
	return ourBackupsNew, err
}

// cleanupOldPeers removes peers that have not been seen in a while
func (sb *SubvolumeBackup) cleanupOldPeers() error {
	entries, err := os.ReadDir(sb.syncDir)
	if err != nil {
		return err
	}

	hour48Ago := time.Now().Add(-48 * time.Hour)
	for _, entry := range entries {
		if !listRegex.MatchString(entry.Name()) {
			continue
		}
		info, err := entry.Info()
		if err != nil {
			return err
		}
		if info.ModTime().Before(hour48Ago) {
			if err := os.RemoveAll(sb.syncDir + "/" + entry.Name()); err != nil {
				return err
			}
		}
	}
	return nil
}

// cleanupOldDiffs removes diffs that are no longer needed by any peer
// over a certain age
func (sb *SubvolumeBackup) cleanupOldDiffs(allUtcList []int64, missingNodeIdxs []int) error {
	entries, err := os.ReadDir(sb.syncDir)
	if err != nil {
		return err
	}

	missingNodeSet := make(map[int64]struct{}, len(missingNodeIdxs))
	for _, missingNodeIdx := range missingNodeIdxs {
		missingNodeSet[allUtcList[missingNodeIdx]] = struct{}{}
	}

	hours48Ago := time.Now().Add(-48 * time.Hour)
	for _, entry := range entries {
		t, err := time.Parse("2006-01-02T1504", entry.Name())
		if err != nil {
			continue
		}
		if hours48Ago.Before(t) {
			// we want to keep the last 48 hours of diffs
			// in-case additional peers come online
			continue
		}
		if _, ok := missingNodeSet[t.Unix()]; !ok {
			path := sb.syncDir + "/" + entry.Name()
			fmt.Printf("Cleaning up old diff dir: %s\n", path)
			if err := os.RemoveAll(path); err != nil {
				return err
			}
		}
	}
	return err
}

// findMissingNodes returns the indexes of the missing nodes
// that are in allUtcList but not in backup
func findMissingNodes(allUtcList []int64, backup []int64) []int {
	cap := len(allUtcList) - len(backup)
	if cap < 0 {
		cap = 0
	}

	missing := make([]int, 0, cap)
	var allIdx, backupIdx int
	for allIdx < len(allUtcList) && backupIdx < len(backup) {
		if allUtcList[allIdx] == backup[backupIdx] {
			allIdx++
			backupIdx++
		} else if allUtcList[allIdx] < backup[backupIdx] { // backup is missing
			missing = append(missing, allIdx)
			allIdx++
		} else { // backup has extra
			backupIdx++
		}
	}
	for ; allIdx < len(allUtcList); allIdx++ {
		missing = append(missing, allIdx)
	}
	return missing
}

var diffRegexp = regexp.MustCompile(`20[0-9]{2}-[01][0-9]-[0-3][0-9]T[0-2][0-9][0-5][0-9]\.btrfs`)

func listDiffs(syncDir, utcStr string) ([]string, []int64, error) {
	entries, err := os.ReadDir(syncDir + "/" + utcStr)
	if errors.Is(err, os.ErrNotExist) {
		return nil, nil, nil
	} else if err != nil {
		return nil, nil, err
	}

	bootstrapFound := false
	diffNames := make([]string, 0, len(entries))
	diffsUtc := make([]int64, 0, len(entries))
	for _, entry := range entries {
		if entry.Name() == "bootstrap.btrfs" {
			bootstrapFound = true
			continue
		}
		if !diffRegexp.MatchString(entry.Name()) {
			continue
		}
		t, err := time.Parse("2006-01-02T1504.btrfs", entry.Name())
		if err != nil {
			continue
		}
		diffNames = append(diffNames, entry.Name())
		diffsUtc = append(diffsUtc, t.Unix())
	}
	sort.Sort(sort.Reverse(sort.StringSlice(diffNames)))
	sort.Sort(sort.Reverse(Int64Sorter(diffsUtc)))
	if bootstrapFound {
		diffNames = append(diffNames, "bootstrap.btrfs")
	}
	return diffNames, diffsUtc, nil
}

func utcToStr(utc int64) string {
	return time.Unix(utc, 0).UTC().Format("2006-01-02T1504")
}

func pathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if errors.Is(err, os.ErrNotExist) {
		return false, nil
	}
	return false, err
}
